// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HeaderDataCpp.h"
#include "GameFramework/GameModeBase.h"
#include "KnifeBaseCpp.h"
#include "FireBaseCpp.h"
#include "Engine/DataTable.h"

#include "GameModeBaseCpp.generated.h"
class AKnifeBaseCpp;
class AFireBaseCpp;


/**
 * 
 */
UCLASS()
class ACTIONGAME_API AGameModeBaseCpp : public AGameModeBase
{
	GENERATED_BODY()
protected:
	FTimerHandle DeadUITimerHandle;

	UPROPERTY()
	TArray<AKnifeBaseCpp*> KnifeObjPool;

	UPROPERTY()
	TArray<AFireBaseCpp*> FireObjPool;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int KnifeObjPoolCapacity;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int FireObjPoolCapacity;

	int Front;
	int Tail;
	int PoolLength;
	int FirePoolLength;
	int FirePoolFront;
	int FirePoolTail;

	int ComboAmount;
	int ComboCnt;

	UPROPERTY()
	FTimerHandle ComboTimerHandle;

	UPROPERTY()
	TSubclassOf<AKnifeBaseCpp> KnifeBP;
	
	UPROPERTY()
	TSubclassOf<AFireBaseCpp> FireBP;

	UPROPERTY()
	TMap<EPawnType, FPawnDamageInfo> DamageMap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UDataTable* DamageTable;

public:
	AGameModeBaseCpp();
	
	void SetDeadUITimer();

	UFUNCTION(BlueprintImplementableEvent)
	void SpawnDroppedItem(const EItemType& SpawnType, const FVector& SpawnLocation);

	UFUNCTION()
	AKnifeBaseCpp* GetKnifeObjFromPool(const FVector& SpawnLocation, const FVector& SpawnDirection);

	UFUNCTION()
	AFireBaseCpp* GetFireObjFromPool(const FVector& SpawnLocation);

	UFUNCTION()
	bool TryPutKnifeObjToPool(AKnifeBaseCpp* Obj);

	UFUNCTION()
	bool TryPutFireObjToPool(AFireBaseCpp* Obj);

	UFUNCTION()
	void StartAttackPro(const int& Amount);

	UFUNCTION()
	bool GetPawnDamageMap(const EPawnType PawnType, const EAttackMode AttackMode, float& Value);
protected:
	void DeadUITimerCallback();
	void AttackProCallback();

	UFUNCTION()
	void ReadDamageTable();
};
