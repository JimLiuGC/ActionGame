// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetStringLibrary.h"
#include "HeaderDataCpp.h"
#include "GameInstanceBaseCpp.generated.h"

class USaveGameBaseCpp;

/**
 * 
 */
UCLASS()
class ACTIONGAME_API UGameInstanceBaseCpp : public UGameInstance
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	UUserWidget* TitleUIWidget;

	UPROPERTY()
	UUserWidget* MenuUIWidget;

	UPROPERTY()
	int TotalReward;

	UPROPERTY()
	USaveGameBaseCpp* GameSaver;

	/** 当前加载的关卡的自定义ID */
	int CurrentMapId;

	/** 当前存档中保存的最大已通过的关卡的自定义ID */
	int MaxMapId;

	UPROPERTY()
	TMap<EItemType, FCollectableItemStruct> PlayerItemMap;

public:
	UFUNCTION(BlueprintCallable)
	UUserWidget* GetTitleUI();

	UFUNCTION(BlueprintCallable)
	void SetTitleUI(UUserWidget* e);

	UFUNCTION(BlueprintCallable)
	UUserWidget* GetMenuUI();

	UFUNCTION(BlueprintCallable)
	void SetMenuUI(UUserWidget* e);

	/**
	* 首先更新最大得分，将总得分和最大解锁关卡的ID存档到存档中，每个关卡结束时会调用。
	*/
	UFUNCTION(BlueprintCallable)
	void SaveGameByGameSaver();

	/**
	 * 从存档中读取总得分和最大解锁关卡的ID。
	 */
	UFUNCTION(BlueprintCallable)
	void LoadGameByGameSaver();

	/**
	 * 返回总得分
	 * @return TotalReward 总得分
	 */
	inline int GetTotalReward() const;

	/**
	 * 关卡结束时将当前关卡的得分更新到总得分里。
	 * @param CurrentMapReward 当前关卡的得分。
	 */
	inline void AddTotalReward(int CurrentMapReward);

	/**
	 * 从存档中读取总得分并赋值，
	 * @param  LoadedTotalReward 从存档中读取的总得分
	 */
	inline void SetTotalReward(int LoadedTotalReward);

	/**
	 * 设置已通过最大关卡的自定义ID。
	 * @param LoadedMapId 已通过最大关卡的自定义ID
	 */
	inline void SetMaxMapId(const int LoadedMapId);

	/**
	 * 返回已通过最大关卡的自定义ID。
	 * @return MaxMapId 已通过最大关卡的自定义ID
	 */
	inline int GetMaxMapId() const;

	/**
	 * 设置当前关卡的自定义ID。
	 * @param LoadedMapId int，当前关卡的自定义ID
	 */
	UFUNCTION(BlueprintCallable)
	void SetCurrentMapId(const int LoadedMapId);

	/**
	 * 返回当前关卡的自定义ID。
	 * @return CurrentMapId int，当前关卡的自定义ID
	 */
	inline int GetCurrentMapId() const;

	/**
	 * 从存档中获取ItemMap
	 * @param LoadedItemMap TMap<EItemType, FCollectableItemStruct>，从存档中读取的ItemMap
	 */
	inline void SetItemMap(TMap<EItemType, FCollectableItemStruct> LoadedItemMap);

	/**
	 * 清除GameInstance中的ItemMap，当且仅当PlayerState从GameInstance中获取到ItemMap后调用
	 */
	inline void ClearItemMap();

	/**
	 * 在外部获取GameInstance中的ItemMap，当且仅当新开关卡后PlayerState初始化ItemMap时调用
	 * @param OutItemMap TMap<EItemType, FCollectableItemStruct>，GameInstance中的ItemMap
	 */
	inline void GetItemMap(TMap<EItemType, FCollectableItemStruct>& OutItemMap) const;

	/**
	 * 根据自定义ID切换关卡。
	 * @param MapId 指定的自定义关卡ID。
	 */
	void LoadMapByMapId(const int MapId);

	/**
	 * 直接返回标题关卡。
	 */
	void BackToTitleMap() const;

	/**
	 * 显示死亡界面
	 */
	void ShowDeadUI();

	UFUNCTION(BlueprintCallable)
	USaveGameBaseCpp* GetGameSaver() const;
	
public:
	UGameInstanceBaseCpp(const FObjectInitializer& ObjectInitializer);
};
