// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HeaderDataCpp.h"
#include "Components/ActorComponent.h"
#include "Kismet/GameplayStatics.h"
#include "InventoryComponentBaseCpp.generated.h"

class ACollectableItemBaseCpp;
class APlayerStateBaseCpp;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ACTIONGAME_API UInventoryComponentBaseCpp : public UActorComponent
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	APlayerStateBaseCpp* CurrentPlayerState;
	
public:	
	// Sets default values for this component's properties
	UInventoryComponentBaseCpp();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	/**
	 * 收集物品的操作，在可收集物品的重叠回调函数里获取玩家的背包组件并调用该函数。该函数将增加收集的物品数量并更新其分类。
	 * @param Item 可收集的物品的基类指针。
	 */
	void Collect(ACollectableItemBaseCpp* Item) const;

	/**
	 * 合成物品的函数。
	 * @param ItemType 源类型
	 * @param MergeCnt int，期望合成目标类型的数量
	 * @param BaseCnt int，基数，一个目标类型需要多少个源类型合成
	 * @return bool，数量是否符合要求，如果符合要求就减少源类型的数量、增加目标类型的数量
	 */
	bool MergeItems(const EItemType ItemType, const int MergeCnt, const int BaseCnt) const;

};
