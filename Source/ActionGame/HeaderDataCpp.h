// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HeaderDataCpp.generated.h"

UENUM(BlueprintType)
enum class EAttackMode : uint8
{ E_BasicAttack, E_ProAttack, E_Attack1, E_Attack2, E_Attack3 };

UENUM(BlueprintType)
enum class EPawnType : uint8
{ E_Player, E_SmallGoblin, E_FlyingEye, E_Demon, E_None };

UENUM(Blueprintable)
enum class EItemType : uint8 { E_SmallDefence, E_BigDefence, E_Power, E_MagicBuffer, E_None };

USTRUCT(BlueprintType)
struct FPawnDamageInfo
{
	GENERATED_BODY()
public:
	UPROPERTY()
	TMap<EAttackMode, float> PawnDamageMap;

	FPawnDamageInfo()
	{
		
	}

	void AddToPawnDamageMap(const EAttackMode Mode, const float Value)
	{
		PawnDamageMap.Add(Mode, Value);
	}
};

/**
* 背包组件内使用的结构体。包含物体名称、物体类别和物体数量。
*/
USTRUCT(BlueprintType)
struct FCollectableItemStruct
{
	GENERATED_USTRUCT_BODY()
private:
	UPROPERTY()
	FString ItemName;

	UPROPERTY()
	EItemType ItemType;
	
	UPROPERTY()
	int ItemCnt;
public:

	/**
	* 默认构造函数
	*/
	FCollectableItemStruct()
	{
		ItemName = FString("");
		ItemType = EItemType::E_None;
		ItemCnt = 0;
	}

	FCollectableItemStruct(const FCollectableItemStruct& Other)
	{
		ItemName = Other.ItemName;
		ItemType = Other.ItemType;
		ItemCnt = Other.ItemCnt;
	}

	/**
	* 有参构造函数
	*/
	FCollectableItemStruct(const FString ItemNameConfig, const EItemType ItemTypeConfig, const int ItemCntConfig)
	{
		ItemName = ItemNameConfig;
		ItemType = ItemTypeConfig;
		ItemCnt = ItemCntConfig;
	}

	/**
	* 使物品数量+1，每收集一个物品就调用一次。
	*/
	void AddCnt()
	{
		ItemCnt++;
	}

	/**
	* 分解物品时调用。
	* @param Delta 分解后子物品增加的数量
	*/
	void AddCnt(const int Delta)
	{
		ItemCnt += Delta;
	}

	/**
	* 合成物品时调用。
	* @param Delta 合成后子物品减少的数量，即合成消耗的数量
	*/
	void RemoveCnt(const int Delta)
	{
		if (ItemCnt - Delta >= 0)
		{
			ItemCnt -= Delta;
		}
	}

	/**
	* 获取物品的数量
	* @return ItemCnt 物品的数量
	*/
	int GetCnt() const
	{
		return ItemCnt;
	}

	/**
	 * 获取物品类别
	 * @return ItemType EItemType，物品类型
	 */
	EItemType GetItemType() const
	{
		return ItemType;
	}
};

/**
 * 组合条目的结构体，该结构体表示某个源类型可以合成的目标类型及其所需源类型的数量。
 * 构成TMap<EItemType SrcType, FCombinationStruct>
 */
USTRUCT(BlueprintType)
struct FCombinationStruct
{
	GENERATED_USTRUCT_BODY()
private:
	UPROPERTY()
	EItemType TargetType;

	UPROPERTY()
	int Amount;

public:
	/**
	 * 默认构造函数
	 */
	FCombinationStruct()
	{
		TargetType = EItemType::E_None;
		Amount = 0;
	}

	/**
	 * 有参构造函数
	 */
	FCombinationStruct(const EItemType& Type, const int& Am)
	{
		TargetType = Type;
		Amount = Am;
	}

	/**
	 * 获取某个组合的目标类型和数量
	 * @param Type EItemType，返回的目标类型
	 * @param Am int，返回的需要的源类型的数量
	 */
	void GetCombination(EItemType& Type, int& Am) const
	{
		Type = TargetType;
		Am = Amount;
	}

	/**
	 * 设置参数
	 */
	void SetCombination(EItemType& Type, int& Am)
	{
		TargetType = Type;
		Amount = Am;
	}
};

/**
 * 
 */
class ACTIONGAME_API HeaderDataCpp
{
public:
	HeaderDataCpp();
	~HeaderDataCpp();
};
