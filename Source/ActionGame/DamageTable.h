// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "CoreMinimal.h"
#include "DamageTable.generated.h"

enum class EAttackMode : uint8;
enum class EPawnType : uint8;

USTRUCT(BlueprintType)
struct FDamageTable : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	EPawnType PawnType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	EAttackMode AttackMode;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float DamageValue;
};

/**
 * 
 */
class ACTIONGAME_API DamageTable
{
public:
	DamageTable();
	~DamageTable();
};
