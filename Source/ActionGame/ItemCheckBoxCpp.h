// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HeaderDataCpp.h"
#include "Blueprint/UserWidget.h"
#include "Components/CheckBox.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"
#include "Kismet/KismetTextLibrary.h"
#include "ItemCheckBoxCpp.generated.h"

class UItemGridCpp;

/**
 * 
 */
UCLASS()
class ACTIONGAME_API UItemCheckBoxCpp : public UUserWidget
{
	GENERATED_BODY()
protected:
	UPROPERTY()
	UItemGridCpp* Parent;

	UPROPERTY()
	UCheckBox* CheckBox;

	UPROPERTY()
	UTextBlock* ItemCntTextBlock;

	UPROPERTY()
	EItemType Type;

	UPROPERTY()
	UImage* UncheckedBGImage;

	UPROPERTY()
	UImage* CheckedBGImage;

	UPROPERTY()
	bool HasItem;
public:
	UItemCheckBoxCpp(const FObjectInitializer& ObjectInitializer);
	virtual bool Initialize() override;
public:
	/**
	 * checkbox状态改变后的回调函数。
	 * 规则：若已经勾选，则再次强制勾选；若未勾选，则将上一个取消勾选，然后更新指针。
	 * 注意：SetIsChecked不会调用该回调函数。
	 * @param State 状态
	 */
	UFUNCTION()
	void OnBoxChanged(bool State);

	/**
	 * 对每个checkbox设置父网格，以便通过父网格控制该checkbox。
	 * @param ParentGrid UItemGridCpp，父网格。
	 */
	void SetParentGrid(UItemGridCpp* ParentGrid);

	/**
	 * 返回checkbox
	 * @return CheckBox UCheckBox。
	 */
	UCheckBox* GetCheckBox() const;

	/**
	 * 设置该checkbox是否含有物品，通常在父网格的Refresh和Reset中使用。若有物品，则启用checkbox，否则禁用checkbox。
	 * @param Has bool，是否含有物品。
	 * @param Cnt int，物品的数量
	 * @param TargetType EItemType，物品类型
	 */
	void SetHasItem(const bool Has, const int Cnt, const EItemType TargetType);

	/**
	 * 返回checkbox是否含有物品。
	 * @return HasItem bool，是否含有物品。
	 */
	bool GetHasItem() const;

	/**
	 * 设置checkbox的Item的样式
	 */
	UFUNCTION(BlueprintImplementableEvent)
	void SetCheckBoxStyle(UCheckBox* Box, EItemType ItemType, const bool Has);

	/**
	 * 设置checkbox的背景，本质上是设置两种背景图的可视性
	 */
	void SetBGImage(const bool State) const;

	/**
	 * 获取当前格子的类型
	 * @return Type EItemType，当前格子的物品类型
	 */
	EItemType GetItemType() const;
};
