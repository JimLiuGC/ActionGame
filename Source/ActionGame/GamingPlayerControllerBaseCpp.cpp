// Fill out your copyright notice in the Description page of Project Settings.


#include "GamingPlayerControllerBaseCpp.h"
#include "PlayerBaseCpp.h"
#include "GameInstanceBaseCpp.h"
#include "GamingUICpp.h"
#include "ItemGridCpp.h"

void AGamingPlayerControllerBaseCpp::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAxis("MoveRight", this, &AGamingPlayerControllerBaseCpp::MoveRightCallback);
	InputComponent->BindAction("MoveJump", IE_Pressed, this, &AGamingPlayerControllerBaseCpp::MoveJumpCallback);
	InputComponent->BindAction("Attack", IE_Pressed, this, &AGamingPlayerControllerBaseCpp::AttackCallback);
	InputComponent->BindAction("Menu", IE_Pressed, this, &AGamingPlayerControllerBaseCpp::MenuCallback);
	InputComponent->BindAction("Back", IE_Pressed, this, &AGamingPlayerControllerBaseCpp::BackCallback);
	InputComponent->BindAction("Inventory", IE_Pressed, this, &AGamingPlayerControllerBaseCpp::InventoryCallback);
	InputComponent->BindAction("AttackPro", IE_Pressed, this, &AGamingPlayerControllerBaseCpp::AttackProCallback);
}

void AGamingPlayerControllerBaseCpp::MoveRightCallback(float Axis)
{
	if (!CheckPanelStack())
	{
		return;
	}
	APawn* PawnBase = GetPawn();
	if (PawnBase == nullptr)
	{
		return;
	}
	APlayerBaseCpp* PlayerBase = Cast<APlayerBaseCpp>(PawnBase);
	if (IsMenuOn || PlayerBase->GetHP() <= 0.0f)
	{
		return;
	}
	if (PlayerBase == nullptr)
	{
		return;
	}
	PlayerBase->MoveRight(Axis);
}

void AGamingPlayerControllerBaseCpp::MoveJumpCallback()
{
	if (!CheckPanelStack())
	{
		return;
	}
	APawn* PawnBase = GetPawn();
	if (PawnBase == nullptr)
	{
		return;
	}
	APlayerBaseCpp* PlayerBase = Cast<APlayerBaseCpp>(PawnBase);
	if (IsMenuOn || PlayerBase->GetHP() <= 0.0f)
	{
		return;
	}
	if (PlayerBase == nullptr)
	{
		return;
	}
	PlayerBase->MoveJump();
}

void AGamingPlayerControllerBaseCpp::AttackCallback()
{
	if (!CheckPanelStack())
	{
		return;
	}
	if (IsMenuOn)
	{
		return;
	}
	APawn* PawnBase = GetPawn();
	if (PawnBase == nullptr)
	{
		return;
	}
	APlayerBaseCpp* PlayerBase = Cast<APlayerBaseCpp>(PawnBase);
	if (PlayerBase == nullptr)
	{
		return;
	}
	PlayerBase->Attack();
}

void AGamingPlayerControllerBaseCpp::AttackProCallback()
{
	if (!CheckPanelStack())
	{
		return;
	}
	if (IsMenuOn)
	{
		return;
	}
	APawn* PawnBase = GetPawn();
	if (PawnBase == nullptr)
	{
		return;
	}
	APlayerBaseCpp* PlayerBase = Cast<APlayerBaseCpp>(PawnBase);
	if (PlayerBase == nullptr)
	{
		return;
	}
	PlayerBase->AttackPro();
}


void AGamingPlayerControllerBaseCpp::MenuCallback()
{
	UGameInstanceBaseCpp* CurrentGameInstance = Cast<UGameInstanceBaseCpp>(GetGameInstance());
	UGamingUICpp* GamingUI = Cast<UGamingUICpp>(CurrentGameInstance->GetMenuUI());
	if (GamingUI != nullptr)
	{
		GamingUI->OpenPanel(GamingUI->GetMenuPanel());
	}
}

void AGamingPlayerControllerBaseCpp::BackCallback()
{
	UGameInstanceBaseCpp* CurrentGameInstance = Cast<UGameInstanceBaseCpp>(GetGameInstance());
	UGamingUICpp* GamingUI = Cast<UGamingUICpp>(CurrentGameInstance->GetMenuUI());
	if (GamingUI != nullptr)
	{
		GamingUI->ClosePanel();
	}
}

void AGamingPlayerControllerBaseCpp::InventoryCallback()
{
	UGameInstanceBaseCpp* CurrentGameInstance = Cast<UGameInstanceBaseCpp>(GetGameInstance());
	UGamingUICpp* GamingUI = Cast<UGamingUICpp>(CurrentGameInstance->GetMenuUI());
	if (GamingUI != nullptr)
	{
		if (CheckPanelStack())
		{
			GamingUI->GetGridManager()->Init();
		}
		GamingUI->OpenPanel(GamingUI->GetInventoryPanel());
	}
}


bool AGamingPlayerControllerBaseCpp::CheckPanelStack() const
{
	UGameInstanceBaseCpp* CurrentGameInstance = Cast<UGameInstanceBaseCpp>(GetGameInstance());
	UGamingUICpp* GamingUI = Cast<UGamingUICpp>(CurrentGameInstance->GetMenuUI());
	if (GamingUI != nullptr)
	{
		if (GamingUI->GetPanelStack().Num() != 0)
		{
			return false;
		}
	}
	return true;
}

