// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HeaderDataCpp.h"
#include "Blueprint/UserWidget.h"
#include "Components/SizeBox.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Kismet/KismetStringLibrary.h"
#include "Kismet/KismetTextLibrary.h"
#include "ItemCombinationPanelCpp.generated.h"

class UItemGridCpp;

/**
 * 
 */
UCLASS()
class ACTIONGAME_API UItemCombinationPanelCpp : public UUserWidget
{
	GENERATED_BODY()
protected:
	UPROPERTY()
	USizeBox* Panel;

	UPROPERTY()
	UTextBlock* AmountTextBlock;

	UPROPERTY()
	UButton* ConfirmButton;

	UPROPERTY()
	UButton* AddCntButton;

	UPROPERTY()
	UButton* SubCntButton;

	UPROPERTY()
	EItemType SourceType;

	UPROPERTY()
	int CntBuffer;

	UPROPERTY()
	int MaxAmount;

	UPROPERTY()
	UItemGridCpp* Parent;

public:
	UItemCombinationPanelCpp(const FObjectInitializer& ObjectInitializer);
	
	virtual bool Initialize() override;
	
	USizeBox* GetPanel() const;

	/**
	 * 设置合成面板中源类型和目标类型的图像
	 * @param SrcType EItemType，源类型
	 * @param TgtType EItemType，目标类型
	 */
	UFUNCTION(BlueprintImplementableEvent)
	void SetSrcAndTgtImage(const EItemType& SrcType, const EItemType& TgtType);

	/**
	 * 设置显示内容，即以“Am/MaxAm”的格式显示。
	 * @param Am int，当前选择的合成数量，一般传入CntBuffer参数
	 * @param MaxAm int，当前的最大合成数量
	 */
	void SetAmountText(const int Am, const int MaxAm) const;

	UFUNCTION()
	void ConfirmButtonClickedCallback();

	/**
	 * 增加合成数量按钮的回调函数
	 */
	UFUNCTION()
	void AddButtonCallback();

	/**
	 * 减少合成数量的回调函数
	 */
	UFUNCTION()
	void SubButtonCallback();

	/**
	 * 复位，根据参数设置面板的显示
	 * @param CurSrcAm int，当前源类型的数量
	 * @param NeedSrcAm int，每合成一个目标类型需要源类型的数量
	 */
	void Reset(const int CurSrcAm, const int NeedSrcAm);

	/**
	 * 设置源类型
	 * @param Type EItemType，源类型
	 */
	void SetSrcType(const EItemType& Type);

	/**
	 * 返回当前选中的源类型
	 * @return SourceType EItemType，当前注册的源类型
	 */
	EItemType GetSrcType() const;

	/**
	 * 设置父组件
	 * @param ItemGrid UItemGridCpp，父组件
	 */
	void SetParent(UItemGridCpp* ItemGrid);
};
