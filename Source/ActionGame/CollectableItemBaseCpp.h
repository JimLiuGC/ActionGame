// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HeaderDataCpp.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "PaperFlipbookComponent.h"
#include "CollectableItemBaseCpp.generated.h"

UCLASS()
class ACTIONGAME_API ACollectableItemBaseCpp : public AActor
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString ItemName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EItemType ItemType;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBoxComponent* BoxCollider;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPaperFlipbookComponent* ItemSprite;
public:	
	// Sets default values for this actor's properties
	ACollectableItemBaseCpp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UFUNCTION()
    void OnPlayerOverlapStart(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
public:
	/**
	 * 获取物品的名称
	 * @return ItemName FString，物品名称
	 */
	FString GetItemName() const;

	/**
	 * 获取物品的类别
	 * @return ItemType EItemType，物品分类
	 */
	EItemType GetItemType() const;
};
