// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBaseCpp.h"
#include "PlayerStateBaseCpp.h"

// Sets default values
AWeaponBaseCpp::AWeaponBaseCpp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collider"));
	BoxCollider->SetSimulatePhysics(false);
	BoxCollider->SetEnableGravity(false);
	BoxCollider->SetGenerateOverlapEvents(true);
	RootComponent = BoxCollider;

	SpriteComponent = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("Sprite"));
	SpriteComponent->SetupAttachment(RootComponent);

	Tags.Add(FName("Weapon"));
}

// Called when the game starts or when spawned
void AWeaponBaseCpp::BeginPlay()
{
	Super::BeginPlay();
	CurrentPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<APlayerStateBaseCpp>();
}

// Called every frame
void AWeaponBaseCpp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeaponBaseCpp::SetWeaponDamage(const float Value)
{
	WeaponDamage = Value;
}

