// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "UIPlayerControllerBaseCpp.generated.h"

/**
 * 
 */
UCLASS()
class ACTIONGAME_API AUIPlayerControllerBaseCpp : public APlayerController
{
	GENERATED_BODY()

public:

	/**
	 * 输入事件“返回”的回调函数，获取视口的UI控件对其执行ClosePanel操作
	 */
	void BackCallback();

public:
	virtual void SetupInputComponent() override;
};
