// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponentBaseCpp.h"
#include "CollectableItemBaseCpp.h"
#include "PlayerStateBaseCpp.h"

// Sets default values for this component's properties
UInventoryComponentBaseCpp::UInventoryComponentBaseCpp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInventoryComponentBaseCpp::BeginPlay()
{
	Super::BeginPlay();

	CurrentPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<APlayerStateBaseCpp>();
	CurrentPlayerState->InitItemMapFromGameInstance();
}


// Called every frame
void UInventoryComponentBaseCpp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UInventoryComponentBaseCpp::Collect(ACollectableItemBaseCpp* Item) const
{
	if (Item->GetItemType() == EItemType::E_None)
	{
		return;
	}
	CurrentPlayerState->AddItemToItemsMap(Item->GetItemType(), 1);
}

bool UInventoryComponentBaseCpp::MergeItems(const EItemType ItemType, const int MergeCnt, const int BaseCnt) const
{
	if (ItemType == EItemType::E_SmallDefence)
	{
		const int NeedCnt = MergeCnt * BaseCnt;
		if (CurrentPlayerState->GetItemCntFromItemsMap(ItemType) < NeedCnt)
		{
			return false;
		}
		CurrentPlayerState->RemoveItemFromItemsMap(ItemType, NeedCnt);
		return true;
	}
	return true;
}
