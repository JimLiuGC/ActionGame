// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "WeaponBaseCpp.h"
#include "Kismet/GameplayStatics.h"
#include "FireBaseCpp.generated.h"

class APlayerBaseCpp;

/**
 * 
 */
UCLASS()
class ACTIONGAME_API AFireBaseCpp : public AWeaponBaseCpp
{
	GENERATED_BODY()
protected:
	UPROPERTY()
	bool Lock;

	UPROPERTY()
	TSet<APlayerBaseCpp*> Players;
	
public:
	AFireBaseCpp();
	virtual void Tick(float DeltaSeconds) override;

	/**
	 * 设置火焰为活跃状态
	 */
	UFUNCTION()
	void SetParam();

	/**
	 * 设置火焰为非活跃状态，在GameMode的TryPutFireObjToPool方法中调用
	 */
	UFUNCTION()
	void SetLock();

	/**
	 * 与玩家重叠时造成一次伤害，但禁用火焰后不会触发玩家离开碰撞盒的事件，所以持续的伤害在Tick中产生。
	 * 维护一个玩家的set，因为联机时可能有多个玩家被火焰攻击
	 */
	UFUNCTION()
    void OnFireOverlapStart(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/**
	 * 玩家离开火焰碰撞盒时触发，从set里移除离开的玩家
	 */
	UFUNCTION()
    void OnFireOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
protected:
	virtual void BeginPlay() override;

	/**
	 * Flipbook播放结束时触发，将该火焰回收到对象池，当且仅当Flipbook设置为不循环时有效
	 */
	UFUNCTION()
	void EndPlayCallback();
};
