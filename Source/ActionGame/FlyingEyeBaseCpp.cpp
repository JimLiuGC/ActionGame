// Fill out your copyright notice in the Description page of Project Settings.


#include "FlyingEyeBaseCpp.h"
#include "NPCAreaBaseCpp.h"

AFlyingEyeBaseCpp::AFlyingEyeBaseCpp()
{
	PrimaryActorTick.bCanEverTick = true;
	ActionState = ENPCAction::E_Idle;
	AnimState = ENPCAnim::E_IdleLeft;
	Tags.Add(FName("FlyingEye"));
}

void AFlyingEyeBaseCpp::BeginPlay()
{
	Super::BeginPlay();
}

void AFlyingEyeBaseCpp::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (locked)
	{
		return;
	}
	if (ActionState == ENPCAction::E_Walk)
	{
		Walk(DeltaSeconds);
	}
	else if (ActionState == ENPCAction::E_Run)
	{
		if (ParentArea != nullptr)
		{
			AActor* Player = ParentArea->GetPlayerInArea();
			if (Player != nullptr)
			{
				Run(DeltaSeconds, Player->GetActorLocation());
			}
		}
	}
}

void AFlyingEyeBaseCpp::DeadTimerCallback()
{
	GetWorldTimerManager().ClearTimer(DeadTimerHandle);
	Destroy();
}

void AFlyingEyeBaseCpp::SetNPCDead()
{
	Super::SetDead();
	const FTimerDelegate DeadTimerDelegate = FTimerDelegate::CreateUObject(this, &AFlyingEyeBaseCpp::DeadTimerCallback);
	GetWorldTimerManager().SetTimer(DeadTimerHandle, DeadTimerDelegate, 0.33336f, false);
}
