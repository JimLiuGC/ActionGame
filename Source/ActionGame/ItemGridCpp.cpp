// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemGridCpp.h"

#include "AttackComponentBaseCpp.h"
#include "CollectableItemBaseCpp.h"
#include "HPComponentBaseCpp.h"
#include "InventoryComponentBaseCpp.h"
#include "ItemCheckBoxCpp.h"
#include "ItemCombinationPanelCpp.h"
#include "PlayerBaseCpp.h"
#include "PlayerStateBaseCpp.h"

UItemGridCpp::UItemGridCpp(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}

bool UItemGridCpp::Initialize()
{
	Super::Initialize();
	
	return true;
}

void UItemGridCpp::Init()
{
	if (ItemArray.Num() != 0)
	{
		Reset();
		Refresh();
		return;
	}
	for (int i = 1; i <= 15; ++i)
	{
		FString WidgetName("ItemCheckBox_");
		WidgetName += UKismetStringLibrary::Conv_IntToString(i);
		UWidget* CheckBoxBase = GetWidgetFromName(UKismetStringLibrary::Conv_StringToName(WidgetName));
		if (CheckBoxBase == nullptr)
		{
			return;
		}
		UItemCheckBoxCpp* CheckBox = Cast<UItemCheckBoxCpp>(CheckBoxBase);
		if (CheckBox == nullptr)
		{
			return;
		}
		CheckBox->SetParentGrid(this);
		ItemArray.Add(CheckBox);
		CheckBox->SetCheckBoxStyle(CheckBox->GetCheckBox(), EItemType::E_SmallDefence, false);
	}
	PrevChecked = ItemArray[0];
	CurrentChecked = PrevChecked;

	UWidget* SelectedItemInfoTextBase = GetWidgetFromName(FName("ItemInfoText"));
	if (SelectedItemInfoTextBase == nullptr)
	{
		return;
	}
	SelectedItemInfoText = Cast<UTextBlock>(SelectedItemInfoTextBase);
	if (SelectedItemInfoText == nullptr)
	{
		return;
	}
	SelectedItemInfoText->SetText(UKismetTextLibrary::Conv_StringToText(FString("")));

	UWidget* CombinationPanelBase = GetWidgetFromName(FName("ItemCombinationPanel"));
	if (CombinationPanelBase == nullptr)
	{
		return;
	}
	CombinationPanel = Cast<UItemCombinationPanelCpp>(CombinationPanelBase);
	if (CombinationPanel == nullptr)
	{
		return;
	}
	CombinationPanel->SetVisibility(ESlateVisibility::Collapsed);
	CombinationPanel->SetParent(this);

	UWidget* UseButtonBase = GetWidgetFromName(FName("UseItemButton"));
	if (UseButtonBase == nullptr)
	{
		return;
	}
	UseButton = Cast<UButton>(UseButtonBase);
	if (UseButton == nullptr)
	{
		return;
	}
	FScriptDelegate UseButtonClickedDelegate;
	UseButtonClickedDelegate.BindUFunction(this, "UseButtonCallback");
	UseButton->OnClicked.Add(UseButtonClickedDelegate);
}


void UItemGridCpp::SetPrevChecked(UItemCheckBoxCpp* Prev)
{
	PrevChecked = Prev;
}

UItemCheckBoxCpp* UItemGridCpp::GetPrevChecked() const
{
	return PrevChecked;
}

void UItemGridCpp::SetCurrChecked(UItemCheckBoxCpp* Curr)
{
	CurrentChecked = Curr;
}

UItemCheckBoxCpp* UItemGridCpp::GetCurrChecked() const
{
	return CurrentChecked;
}

void UItemGridCpp::Reset()
{
	APlayerStateBaseCpp* CurrentPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<APlayerStateBaseCpp>();
	for (UItemCheckBoxCpp* Box : ItemArray)
	{
		if (Box->GetHasItem())
		{
			Box->GetCheckBox()->SetIsChecked(false);
			Box->SetHasItem(false, 0, EItemType::E_None);
		}
	}
	int i = 0;
	const TMap<EItemType, FCollectableItemStruct>* Items = CurrentPlayerState->GetItemsMap();
	if (Items == nullptr)
	{
		return;
	}
	if (Items->Num() != 0)
	{
		UseButton->SetIsEnabled(true);
		for (const TTuple<EItemType, FCollectableItemStruct> E : *Items)
		{
			ItemArray[i]->SetHasItem(true, E.Value.GetCnt(), E.Key);
			i++;
		}
		CurrentChecked = ItemArray[0];
		PrevChecked = CurrentChecked;
	}
	else
	{
		UseButton->SetIsEnabled(false);
	}
}

void UItemGridCpp::Refresh()
{
	APlayerStateBaseCpp* CurrentPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<APlayerStateBaseCpp>();
	const TMap<EItemType, FCollectableItemStruct>* Items = CurrentPlayerState->GetItemsMap();
	if (Items == nullptr)
	{
		return;
	}
	if (CurrentChecked->GetHasItem())
	{
		CurrentChecked->GetCheckBox()->SetIsChecked(true);
		CurrentChecked->SetBGImage(true);
		SetItemInfoText(CurrentChecked->GetItemType());
		if (CurrentPlayerState->HasCombination(CurrentChecked->GetItemType()))
		{
			CombinationPanel->SetVisibility(ESlateVisibility::Visible);
			const EItemType CurrentType = CurrentChecked->GetItemType();
			EItemType TgtType;
			int Amount;
			CurrentPlayerState->GetCombinationMap(CurrentType, TgtType, Amount);
			const int CurAmount = CurrentPlayerState->GetItemCntFromItemsMap(CurrentType);
			CombinationPanel->SetSrcAndTgtImage(CurrentType, TgtType);
			CombinationPanel->Reset(CurAmount, Amount);
		}
		else
		{
			CombinationPanel->SetVisibility(ESlateVisibility::Collapsed);
		}
	}
	else
	{
		CurrentChecked->GetCheckBox()->SetIsChecked(false);
		CurrentChecked->SetBGImage(false);
		SetItemInfoText(EItemType::E_None);
		CombinationPanel->SetVisibility(ESlateVisibility::Collapsed);
	}
}

UItemCombinationPanelCpp* UItemGridCpp::GetItemCombinationPanel() const
{
	return CombinationPanel;
}

void UItemGridCpp::UseButtonCallback()
{
	const EItemType UsedType = CurrentChecked->GetItemType();
	APlayerStateBaseCpp* CurrentPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<APlayerStateBaseCpp>();
	const bool Flag = CurrentPlayerState->RemoveItemFromItemsMap(UsedType, 1);
	if (!Flag)
	{
		CurrentChecked->SetHasItem(false, 0, EItemType::E_None);
		CurrentChecked->SetBGImage(false);
	}
	APlayerBaseCpp* CurrentPlayer = Cast<APlayerBaseCpp>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if (CurrentPlayer == nullptr)
	{
		return;
	}
	switch (UsedType)
	{
	case EItemType::E_SmallDefence:
		break;
	case EItemType::E_BigDefence:
		{
			CurrentPlayer->GetHPComponent()->SetEnableDefence(true);
		}
	case EItemType::E_Power:
		{
			CurrentPlayerState->IncreasePower(30.0f);
			break;
		}
	case EItemType::E_MagicBuffer:
		{
			CurrentPlayer->GetAttackComponent()->SetUseMagicBuffer(true);
		}
	case EItemType::E_None:
		break;
	default:
		break;
	}
	Reset();
	Refresh();
}

