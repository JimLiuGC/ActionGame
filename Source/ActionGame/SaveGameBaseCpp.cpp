// Fill out your copyright notice in the Description page of Project Settings.


#include "SaveGameBaseCpp.h"


USaveGameBaseCpp::USaveGameBaseCpp()
{
	SlotName = FString("GameSaveSlot");
}

void USaveGameBaseCpp::SaveData(const int& SavedMapId, const float& CurrentPlayerHP, const TMap<EItemType, FCollectableItemStruct>& SavedItemsMap)
{
	MapId = SavedMapId;
	ItemsMap = SavedItemsMap;
	PlayerHP = CurrentPlayerHP;
	UGameplayStatics::SaveGameToSlot(this, SlotName, 0);
}

int USaveGameBaseCpp::GetReward() const
{
	return Reward;
}

int USaveGameBaseCpp::GetMapId() const
{
	return MapId;
}

void USaveGameBaseCpp::GetSavedItemsMap(TMap<EItemType, FCollectableItemStruct>& OutItemMap) const
{
	OutItemMap =  ItemsMap;
}

