// Fill out your copyright notice in the Description page of Project Settings.

#include "GameInstanceBaseCpp.h"
#include "PlayerStateBaseCpp.h"
#include "GamingUICpp.h"
#include "SaveGameBaseCpp.h"
#include "PlayerBaseCpp.h"
#include "TitleUICpp.h"

UGameInstanceBaseCpp::UGameInstanceBaseCpp(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	GameSaver = CreateDefaultSubobject<USaveGameBaseCpp>(TEXT("Game Saver"));
	CurrentMapId = 0;
	MaxMapId = 0;
	USaveGame* GameLoaderBase = UGameplayStatics::LoadGameFromSlot(FString("GameSaveSlot"), 0);
	if (GameLoaderBase == nullptr)
	{
		TMap<EItemType, FCollectableItemStruct> ItemsMap;
		/** 将标题关卡编号设为0，即，存档中的关卡编号是已经完成的关卡编号，关卡选择界面应解锁存档关卡编号+1的关卡 */
		GameSaver->SaveData(0, 0, ItemsMap);
	}
}

UUserWidget* UGameInstanceBaseCpp::GetTitleUI()
{
	return TitleUIWidget;
}

void UGameInstanceBaseCpp::SetTitleUI(UUserWidget* e)
{
	TitleUIWidget = e;
}

UUserWidget* UGameInstanceBaseCpp::GetMenuUI()
{
	return MenuUIWidget;
}

void UGameInstanceBaseCpp::SetMenuUI(UUserWidget* e)
{
	MenuUIWidget = e;
}

void UGameInstanceBaseCpp::AddTotalReward(int CurrentMapReward)
{
	TotalReward += CurrentMapReward;
}

int UGameInstanceBaseCpp::GetTotalReward() const
{
	return TotalReward;
}

void UGameInstanceBaseCpp::SetTotalReward(int LoadedTotalReward)
{
	TotalReward = LoadedTotalReward;
}

void UGameInstanceBaseCpp::SetMaxMapId(const int LoadedMapId)
{
	MaxMapId = LoadedMapId;
}

int UGameInstanceBaseCpp::GetMaxMapId() const
{
	return MaxMapId;
}

void UGameInstanceBaseCpp::SetCurrentMapId(const int LoadedMapId)
{
	CurrentMapId = LoadedMapId;
}

int UGameInstanceBaseCpp::GetCurrentMapId() const
{
	return CurrentMapId;
}

void UGameInstanceBaseCpp::SetItemMap(TMap<EItemType, FCollectableItemStruct> LoadedItemMap)
{
	for (auto E : LoadedItemMap)
	{
		FCollectableItemStruct FS(E.Value);
		PlayerItemMap.Add(E.Key, FS);
	}
}

void UGameInstanceBaseCpp::ClearItemMap()
{
	PlayerItemMap.Reset();
}

void UGameInstanceBaseCpp::GetItemMap(TMap<EItemType, FCollectableItemStruct>& OutItemMap) const
{
	OutItemMap = PlayerItemMap;
}

void UGameInstanceBaseCpp::LoadMapByMapId(const int MapId) 
{
	FString MapNameStr("Map_");
	MapNameStr += UKismetStringLibrary::Conv_IntToString(MapId);
	UGameplayStatics::OpenLevel(GetWorld(), UKismetStringLibrary::Conv_StringToName(MapNameStr));
}

void UGameInstanceBaseCpp::SaveGameByGameSaver()
{
	if (GameSaver == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("GameSaver == nullptr"));
		return;
	}
	APlayerBaseCpp* CurrentPlayer = Cast<APlayerBaseCpp>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (CurrentPlayer == nullptr)
	{
		return;
	}
	APlayerStateBaseCpp* CurrentPlayerState = Cast<APlayerStateBaseCpp>(CurrentPlayer->GetPlayerState());
	if (CurrentPlayerState == nullptr)
	{
		return;
	}
	if (CurrentMapId <= MaxMapId)
	{
		UE_LOG(LogTemp, Warning, TEXT("CurrentMapId <= MaxMapId"));
		GameSaver->SaveData(MaxMapId, CurrentPlayerState->GetPlayerHP(), *CurrentPlayerState->GetItemsMap());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("CurrentMapId %d"), CurrentMapId);
		GameSaver->SaveData(CurrentMapId, CurrentPlayerState->GetPlayerHP(), *CurrentPlayerState->GetItemsMap());
	}
}

void UGameInstanceBaseCpp::LoadGameByGameSaver()
{
	USaveGame* GameLoaderBase = UGameplayStatics::LoadGameFromSlot(FString("GameSaveSlot"), 0);
	USaveGameBaseCpp* GameLoader = Cast<USaveGameBaseCpp>(GameLoaderBase);
	MaxMapId = GameLoader->GetMapId();
	PlayerItemMap.Reset();
	TMap<EItemType, FCollectableItemStruct> OutItemMap;
	GameLoader->GetSavedItemsMap(OutItemMap);
	SetItemMap(OutItemMap);
}

void UGameInstanceBaseCpp::BackToTitleMap() const
{
	UGameplayStatics::OpenLevel(GetWorld(), FName("MapTitle"));
}

void UGameInstanceBaseCpp::ShowDeadUI()
{
	if (MenuUIWidget == nullptr)
	{
		return;
	}
	UGamingUICpp* GamingUIWidget = Cast<UGamingUICpp>(MenuUIWidget);
	if (GamingUIWidget == nullptr)
	{
		return;
	}
	GamingUIWidget->ShowDeadPanel();
}

USaveGameBaseCpp* UGameInstanceBaseCpp::GetGameSaver() const
{
	return GameSaver;
}

