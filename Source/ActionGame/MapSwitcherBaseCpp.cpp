// Fill out your copyright notice in the Description page of Project Settings.


#include "MapSwitcherBaseCpp.h"
#include "GameInstanceBaseCpp.h"

// Sets default values
AMapSwitcherBaseCpp::AMapSwitcherBaseCpp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BoxOverlapper = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	BoxOverlapper->SetSimulatePhysics(false);
	BoxOverlapper->SetGenerateOverlapEvents(true);
	BoxOverlapper->SetEnableGravity(false);
	RootComponent = BoxOverlapper;
}

// Called when the game starts or when spawned
void AMapSwitcherBaseCpp::BeginPlay()
{
	Super::BeginPlay();
	FScriptDelegate SwitcherOverlapStartDelegate;
	SwitcherOverlapStartDelegate.BindUFunction(this, "OnSwitcherOverlapStart");
	BoxOverlapper->OnComponentBeginOverlap.Add(SwitcherOverlapStartDelegate);

	FScriptDelegate SwitcherOverlapEndDelegate;
	SwitcherOverlapEndDelegate.BindUFunction(this, "OnSwitcherOverlapEnd");
	BoxOverlapper->OnComponentEndOverlap.Add(SwitcherOverlapEndDelegate);
}

// Called every frame
void AMapSwitcherBaseCpp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMapSwitcherBaseCpp::OnSwitcherOverlapStart(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->ActorHasTag(FName("Player")))
	{
		UGameInstanceBaseCpp* CurrentGameInstance = Cast<UGameInstanceBaseCpp>(GetGameInstance());
		CurrentGameInstance->SaveGameByGameSaver();
		UE_LOG(LogTemp, Warning, TEXT("Switch"));
		CurrentGameInstance->LoadMapByMapId(CurrentGameInstance->GetCurrentMapId() + 1);
	}
}

void AMapSwitcherBaseCpp::OnPSwitcherOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//
}

