// Fill out your copyright notice in the Description page of Project Settings.


#include "TitleUICpp.h"
#include "GameInstanceBaseCpp.h"
#include "MapButtonCpp.h"

#include <string>

UTitleUICpp::UTitleUICpp(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}

bool UTitleUICpp::Initialize()
{
	Super::Initialize();

	UWidget* StartBtnBase = GetWidgetFromName(FName("StartButton"));
	if (StartBtnBase == nullptr)
	{
		return false;
	}
	UButton* StartBtn = Cast<UButton>(StartBtnBase);
	if (StartBtn == nullptr)
	{
		return false;
	}

	UWidget* InfoBtnBase = GetWidgetFromName(FName("InfoButton"));
	if (InfoBtnBase == nullptr)
	{
		return false;
	}
	UButton* InfoBtn = Cast<UButton>(InfoBtnBase);
	if (InfoBtn == nullptr)
	{
		return false;
	}

	UWidget* ExitBtnBase = GetWidgetFromName(FName("ExitButton"));
	if (ExitBtnBase == nullptr)
	{
		return false;
	}
	UButton* ExitBtn = Cast<UButton>(ExitBtnBase);
	if (ExitBtn == nullptr)
	{
		return false;
	}

	FScriptDelegate StartBtnDelegate;
	StartBtnDelegate.BindUFunction(this, FName("OnStartBtnCallback"));
	StartBtn->OnClicked.Add(StartBtnDelegate);

	FScriptDelegate InfoBtnDelegate;
	InfoBtnDelegate.BindUFunction(this, FName("OnInfoBtnCallback"));
	InfoBtn->OnClicked.Add(InfoBtnDelegate);

	FScriptDelegate ExitBtnDelegate;
	ExitBtnDelegate.BindUFunction(this, FName("OnExitBtnCallback"));
	ExitBtn->OnClicked.Add(ExitBtnDelegate);

	UWidget* MainPanelBase = GetWidgetFromName(FName("MainPanel"));
	if (MainPanelBase == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Panel Name Wrong"));
		return false;
	}
	MainPanel = Cast<UCanvasPanel>(MainPanelBase);
	if (MainPanel == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Cast Error"));
		return false;
	}

	UWidget* InfoPanelBase = GetWidgetFromName(FName("InfoPanel"));
	if (InfoPanelBase == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Panel Name Wrong"));
		return false;
	}
	InfoPanel = Cast<UCanvasPanel>(InfoPanelBase);
	if (InfoPanel == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Cast Error"));
		return false;
	}
	InfoPanel->SetVisibility(ESlateVisibility::Collapsed);

	UWidget* MapPanelBase = GetWidgetFromName(FName("MapPanel"));
	if (MapPanelBase == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Panel Name Wrong"));
		return false;
	}
	MapPanel = Cast<UCanvasPanel>(MapPanelBase);
	if (MapPanel == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Cast Error"));
		return false;
	}
	MapPanel->SetVisibility(ESlateVisibility::Collapsed);

	for (int i = 1; i <= 5; i++)
	{
		UWidget* MapBtnBase = GetWidgetFromName(UKismetStringLibrary::Conv_StringToName(FString("Map_") + UKismetStringLibrary::Conv_IntToString(i)));
		if (MapBtnBase == nullptr)
		{
			UE_LOG(LogTemp, Error, TEXT("Widget name wrong"));
			return false;
		}
		UMapButtonCpp* MapBtn = Cast<UMapButtonCpp>(MapBtnBase);
		if (MapBtn == nullptr)
		{
			UE_LOG(LogTemp, Error, TEXT("Cast error"));
			return false;
		}
		MapBtn->SetMapId(i);
		MapBtnArray.Add(MapBtn);
	}
	return true;
}

void UTitleUICpp::OnStartBtnCallback()
{
	OpenPanel(GetMapPanel());
	UGameInstanceBaseCpp* CurrentGameInstance = Cast<UGameInstanceBaseCpp>(GetGameInstance());
	CurrentGameInstance->LoadGameByGameSaver();
	const int MapId = CurrentGameInstance->GetMaxMapId();
	for (auto Btn : MapBtnArray)
	{
		/** 当前关卡编号+1是因为关卡选择界面应激活存档关卡编号+1 */
		if (Btn->GetMapId() <= MapId + 1)
		{
			Btn->SetIsEnabled(true);
		}
		else
		{
			Btn->SetIsEnabled(false);
		}
	}
}

void UTitleUICpp::OnInfoBtnCallback()
{
	OpenPanel(GetInfoPanel());
}

void UTitleUICpp::OnExitBtnCallback()
{
	UKismetSystemLibrary::QuitGame(this, nullptr, EQuitPreference::Quit, true);
}

void UTitleUICpp::OpenPanel(UCanvasPanel* TargetPanel)
{
	if (TargetPanel == nullptr)
	{
		return;
	}
	TargetPanel->SetVisibility(ESlateVisibility::Visible);
	if (PanelStack.Num() == 0 || PanelStack.Last() != TargetPanel)
	{
		PanelStack.Emplace(TargetPanel);
	}
}

void UTitleUICpp::ClosePanel()
{
	if (PanelStack.Num() != 0)
	{
		UCanvasPanel* TopPanel = PanelStack.Last();
		PanelStack.Pop();
		if (TopPanel != nullptr)
		{
			TopPanel->SetVisibility(ESlateVisibility::Collapsed);
		}
		TopPanel = nullptr;
	}
}

UCanvasPanel* UTitleUICpp::GetInfoPanel() const
{
	return InfoPanel;
}

UCanvasPanel* UTitleUICpp::GetMapPanel() const
{
	return MapPanel;
}

TArray<UCanvasPanel*> UTitleUICpp::GetPanelStack() const
{
	return PanelStack;
}
