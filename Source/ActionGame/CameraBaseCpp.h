// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "PlayerBaseCpp.h"
#include "GameFramework/Actor.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/SpringArmComponent.h"
#include "CameraBaseCpp.generated.h"

UCLASS()
class ACTIONGAME_API ACameraBaseCpp : public AActor
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	APlayerBaseCpp* LocalPlayer;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UCameraComponent* FollowCamera;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USpringArmComponent* CameraArm;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RFromCenter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float XFromPlayer;

public:	
	// Sets default values for this actor's properties
	ACameraBaseCpp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
