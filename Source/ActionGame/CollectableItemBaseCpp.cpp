// Fill out your copyright notice in the Description page of Project Settings.


#include "CollectableItemBaseCpp.h"
#include "PlayerBaseCpp.h"
#include "InventoryComponentBaseCpp.h"
// Sets default values
ACollectableItemBaseCpp::ACollectableItemBaseCpp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Coliider"));
	BoxCollider->SetSimulatePhysics(false);
	BoxCollider->SetEnableGravity(false);
	BoxCollider->SetGenerateOverlapEvents(true);
	RootComponent = BoxCollider;

	ItemSprite = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("Item Sprite"));
	ItemSprite->SetupAttachment(RootComponent);

	Tags.Add(FName("CollectableItem"));
}

// Called when the game starts or when spawned
void ACollectableItemBaseCpp::BeginPlay()
{
	Super::BeginPlay();
	FScriptDelegate ItemOverlapStartDelegate;
	ItemOverlapStartDelegate.BindUFunction(this, "OnPlayerOverlapStart");
	BoxCollider->OnComponentBeginOverlap.Add(ItemOverlapStartDelegate);
}

// Called every frame
void ACollectableItemBaseCpp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FString ACollectableItemBaseCpp::GetItemName() const
{
	return ItemName;
}

EItemType ACollectableItemBaseCpp::GetItemType() const
{
	return ItemType;
}

void ACollectableItemBaseCpp::OnPlayerOverlapStart(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->ActorHasTag(FName("Player")))
	{
		APlayerBaseCpp* Player = Cast<APlayerBaseCpp>(OtherActor);
		Player->GetInventoryComponent()->Collect(this);
		Destroy();
	}
}

