// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemCombinationPanelCpp.h"
#include "PlayerStateBaseCpp.h"

UItemCombinationPanelCpp::UItemCombinationPanelCpp(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}

bool UItemCombinationPanelCpp::Initialize()
{
	Super::Initialize();
	UWidget* AmountTextBlockBase = GetWidgetFromName(FName("AmountText"));
	if (AmountTextBlockBase == nullptr)
	{
		return false;
	}
	AmountTextBlock = Cast<UTextBlock>(AmountTextBlockBase);
	if (AmountTextBlock == nullptr)
	{
		return false;
	}
	AmountTextBlock->SetText(UKismetTextLibrary::Conv_StringToText(FString("")));

	UWidget* ConfirmCombineButtonBase = GetWidgetFromName(FName("ConfirmCombineButton"));
	if (ConfirmCombineButtonBase == nullptr)
	{
		return false;
	}
	ConfirmButton = Cast<UButton>(ConfirmCombineButtonBase);
	if (ConfirmButton == nullptr)
	{
		return false;
	}
	FScriptDelegate ConfirmButtonDelegate;
	ConfirmButtonDelegate.BindUFunction(this, "ConfirmButtonClickedCallback");
	ConfirmButton->OnClicked.Add(ConfirmButtonDelegate);

	UWidget* AddCntButtonBase = GetWidgetFromName(FName("AddButton"));
	if (AddCntButtonBase == nullptr)
	{
		return false;
	}
	AddCntButton = Cast<UButton>(AddCntButtonBase);
	if (AddCntButton == nullptr)
	{
		return false;
	}
	FScriptDelegate AddCntButtonDelegate;
	AddCntButtonDelegate.BindUFunction(this, "AddButtonCallback");
	AddCntButton->OnClicked.Add(AddCntButtonDelegate);
	
	UWidget* SubCntButtonBase = GetWidgetFromName(FName("SubButton"));
	if (SubCntButtonBase == nullptr)
	{
		return false;
	}
	SubCntButton = Cast<UButton>(SubCntButtonBase);
	if (SubCntButton == nullptr)
	{
		return false;
	}
	FScriptDelegate SubCntButtonDelegate;
	SubCntButtonDelegate.BindUFunction(this, "SubButtonCallback");
	SubCntButton->OnClicked.Add(SubCntButtonDelegate);
	return true;
}


USizeBox* UItemCombinationPanelCpp::GetPanel() const
{
	return Panel;
}

void UItemCombinationPanelCpp::SetAmountText(const int Am, const int MaxAm) const
{
	const FString StrAm = UKismetStringLibrary::Conv_IntToString(Am);
	const FString StrMaxAm = UKismetStringLibrary::Conv_IntToString(MaxAm);
	const FString Str = StrAm + FString("/") + StrMaxAm;
	AmountTextBlock->SetText(UKismetTextLibrary::Conv_StringToText(Str));
}

void UItemCombinationPanelCpp::ConfirmButtonClickedCallback()
{
	if (CntBuffer == 0)
	{
		return;
	}
	APlayerStateBaseCpp* CurrentPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<APlayerStateBaseCpp>();
	if (CurrentPlayerState == nullptr)
	{
		return;
	}
	EItemType TargetType;
	int Amount;
	CurrentPlayerState->GetCombinationMap(SourceType, TargetType, Amount);
	const int RemovedSrcCnt = Amount * CntBuffer;
	CurrentPlayerState->RemoveItemFromItemsMap(SourceType, RemovedSrcCnt);
	CurrentPlayerState->AddItemToItemsMap(TargetType, CntBuffer);
	Parent->Reset();
	Parent->Refresh();
}

void UItemCombinationPanelCpp::AddButtonCallback()
{
	if (CntBuffer + 1 <= MaxAmount)
	{
		CntBuffer++;
		SetAmountText(CntBuffer, MaxAmount);
	}
}

void UItemCombinationPanelCpp::SubButtonCallback()
{
	if (CntBuffer - 1 >= 0)
	{
		CntBuffer--;
		SetAmountText(CntBuffer, MaxAmount);
	}
}

void UItemCombinationPanelCpp::Reset(const int CurSrcAm, const int NeedSrcAm)
{
	const int MaxCombineAmount = CurSrcAm / NeedSrcAm;
	CntBuffer = 0;
	MaxAmount = MaxCombineAmount;
	SetAmountText(CntBuffer, MaxCombineAmount);
}

void UItemCombinationPanelCpp::SetSrcType(const EItemType& Type)
{
	SourceType = Type;
}

EItemType UItemCombinationPanelCpp::GetSrcType() const
{
	return SourceType;
}

void UItemCombinationPanelCpp::SetParent(UItemGridCpp* ItemGrid)
{
	Parent = ItemGrid;
}

