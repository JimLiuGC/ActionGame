// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Kismet/KismetTextLibrary.h"
#include "MapButtonCpp.generated.h"

/**
 * 
 */
UCLASS()
class ACTIONGAME_API UMapButtonCpp : public UUserWidget
{
	GENERATED_BODY()
protected:
	int MapId;
public:
	UMapButtonCpp(const FObjectInitializer& ObjectInitializer);

	virtual bool Initialize() override;

public:
	UFUNCTION()
	void OnMapBtnCallback();

	UFUNCTION(BlueprintCallable)
	void SetMapId(const int ID);
	
	inline int GetMapId() const;
};
