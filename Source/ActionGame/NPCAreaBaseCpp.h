/**
 * NPC所属区域的基类。
 */
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "NPCAreaBaseCpp.generated.h"

class ANPCBaseCpp;

UCLASS()
class ACTIONGAME_API ANPCAreaBaseCpp : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBoxComponent* Area;
protected:
	UPROPERTY()
	TMap<ANPCBaseCpp*, bool> NPCsState;

	UPROPERTY()
	AActor* Player;

	float W;
	float H;
public:	
	// Sets default values for this actor's properties
	ANPCAreaBaseCpp();

	/**
	 * 将在关卡蓝图中生成的NPC注册到存活状态字典中。
	 */
	inline void RegisterToNPCsState(ANPCBaseCpp* const NPC);

	/**
	 * 将NPC在存活状态字典中标记为死亡。
	 */
	inline void UpdateNPCsState(const ANPCBaseCpp* NPC);

	/**
	 * 返回位于NPC所属区域中的玩家角色。
	 * @return Player AActor，NPC所属区域中的玩家角色
	 */
	inline AActor* GetPlayerInArea() const;

	/**
	 * 设置所属区域的尺寸，是在默认尺寸基础上进行边界的扩充，在蓝图中生成所属区域时调用。
	 */
	UFUNCTION(BlueprintCallable)
	void SetAreaSize(const float WConfig, const float HConfig);

	/**
	 * 玩家角色离开所属区域时调用。该函数通知存活状态中存活的NPC改变状态。
	 */
	void Notify(const bool PlayerInArea);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION()
	void OnPlayerOverlapStart(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnPlayerOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

};
