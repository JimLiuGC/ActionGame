// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "PaperFlipbookComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MovablePlatformBaseCpp.generated.h"

UCLASS()
class ACTIONGAME_API AMovablePlatformBaseCpp : public AActor
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBoxComponent* BoxCollider;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBoxComponent* DeathCollider;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPaperFlipbookComponent* SpriteComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MoveSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector MoveDirection;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector2D Border;
public:	
	// Sets default values for this actor's properties
	AMovablePlatformBaseCpp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
    void OnPlayerOverlapStart(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
