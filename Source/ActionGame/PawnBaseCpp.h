/**
 * 所有角色的基类，包括玩家角色和NP角色.
 * 该类有其派生类可以共用的方法和属性。
 */

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PaperFlipbookComponent.h"
#include "Components/BoxComponent.h"
#include "PawnBaseCpp.generated.h"

UCLASS()
class ACTIONGAME_API APawnBaseCpp : public APawn
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MoveSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbookComponent* SpriteFlipBook;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* BoxCollider;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* BlockBoxCollider;

private:
	UPROPERTY()
	float HP;

	UPROPERTY()
	float CurrentHP;

public:
	// Sets default values for this pawn's properties
	APawnBaseCpp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/**
	 * 单个的射线检测，派生类可直接复用
	 * @param StartPoint - 射线起点
	 * @param EndPoint - 射线终点
	 * @param HitResult - 检测结果
	 */
	virtual void RayTrace(const FVector& StartPoint, const FVector EndPoint, FHitResult& HitResult);

	/**
	 * 多个的射线检测，派生类可直接复用
	 * @param StartPoint - 射线起点
	 * @param EndPoint - 射线终点
	 * @param HitResults - 检测结果列表
	 */
	virtual void RayTraceMulti(const FVector& StartPoint, const FVector EndPoint, TArray<FHitResult>& HitResults);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/**
	 * 返回当前HP值，子类可直接复用。
	 * @return CurrentHP 返回当前的HP值。
	 */
	inline float GetCurrentHP() const;

	/**
	 * 返回总共的HP值
	 * @return HP 初始HP值
	 */
	inline float GetHP() const;

	/**
	 * 设置初始HP
	 * @param InitHP float，初始HP值
	 */
	inline void SetInitHP(const float InitHP);

	/**
	 * 减少当前HP
	 * @param DeltaValue float，当前HP的减少量
	 */
	inline void DecreaseCurrentHP(const float DeltaValue);
};
