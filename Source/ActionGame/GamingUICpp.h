// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/CanvasPanel.h"
#include "Kismet/KismetTextLibrary.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Components/ProgressBar.h"
#include "GamingUICpp.generated.h"

class UItemGridCpp;
class APlayerStateBaseCpp;
class ADemonBaseCpp;

/**
 * 
 */
UCLASS()
class ACTIONGAME_API UGamingUICpp : public UUserWidget
{
	GENERATED_BODY()
protected:
	UPROPERTY()
	UTextBlock* RewardText;

	UPROPERTY()
	UCanvasPanel* MenuPanel;

	UPROPERTY()
	UCanvasPanel* InventoryPanel;

	UPROPERTY()
	UCanvasPanel* DeadPanel;

	UPROPERTY()
	UProgressBar* PowerSlider;

	UPROPERTY()
	UProgressBar* HPSlider;

	UPROPERTY()
	UProgressBar* NPCHPSlider;

	UPROPERTY()
	TArray<UCanvasPanel*> PanelStack;

	UPROPERTY()
	UItemGridCpp* GridManager;

	UPROPERTY()
	APlayerStateBaseCpp* CurrentPlayerState;

	UPROPERTY()
	ADemonBaseCpp* Demon;

public:
	void ShowReward();

	/**
	 * 显示死亡信息面板，显示之后重新载入关卡，故不需要使用面板栈
	 */
	inline void ShowDeadPanel() const;

	/**
	 * 打开指定的面板，对面板栈进行操作
	 * @param TargetPanel 需要打开的面板，由GetXXXPanel获得
	 */
	inline void OpenPanel(UCanvasPanel* TargetPanel);

	/**
	 * 关闭最上层的面板，对面板栈进行操作。
	 */
	inline void ClosePanel();

	/**
	 * 返回首页按钮的回调函数
	 */
	UFUNCTION()
	void OnBackBtnCallback() const;

	/**
	 * 从PlayerState获取力量值，该函数为力量条的回调函数
	 * @return Power 力量值
	 */
	UFUNCTION()
	float GetPowerFromPlayerState();

	/**
	 * 从PlayerState获取HP，该函数为血条的回调函数
	 * @return HP 血量
	 */
	UFUNCTION()
	float GetHPFromPlayerState();

	UFUNCTION()
	float GetNPCHP();

	/**
	 * 返回菜单面板
	 * @return MenuPanel 菜单面板
	 */
	inline UCanvasPanel* GetMenuPanel() const;

	/**
	 * 返回背包面板
	 * @return InventoryPanel 背包面板
	 */
	inline UCanvasPanel* GetInventoryPanel() const;

	/**
	 * 返回网格管理器
	 * @return GridManager 网格管理器
	 */
	inline UItemGridCpp* GetGridManager() const;

	/**
	 * 返回面板栈
	 * @return PanelStack 返回面板栈
	 */
	inline TArray<UCanvasPanel*> GetPanelStack() const;

	inline UProgressBar* GetNPCHPSlider() const;

	inline void SetDemon(AActor* DemonActor);

public:
	virtual bool Initialize() override;
};
