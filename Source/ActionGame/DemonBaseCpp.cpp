// Fill out your copyright notice in the Description page of Project Settings.

#include "DemonBaseCpp.h"
#include "GameModeBaseCpp.h"
#include "GameInstanceBaseCpp.h"
#include "GamingUICpp.h"

ADemonBaseCpp::ADemonBaseCpp()
{
	PrimaryActorTick.bCanEverTick = true;
	Lock = false;
	Tags.Add(FName("Demon"));
}

void ADemonBaseCpp::BeginPlay()
{
	Super::BeginPlay();
	CurrentGameMode = Cast<AGameModeBaseCpp>(UGameplayStatics::GetGameMode(GetWorld()));
}

void ADemonBaseCpp::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	if (ActionState == ENPCAction::E_Attack1)
	{
		if (SpriteFlipBook->GetPlaybackPositionInFrames() == 6)
		{
			if (!Lock)
			{
				const FVector FireLocation = GetActorLocation() + FVector(-100.0f, 0.0f, -80.0f);
				CurrentGameMode->GetFireObjFromPool(FireLocation);
				Lock = true;
			}
		}
		else
		{
			Lock = false;
		}
	}
}

void ADemonBaseCpp::SetNPCDead()
{
	Super::SetDead();
	UGameInstanceBaseCpp* CurrentGameInstance = Cast<UGameInstanceBaseCpp>(GetGameInstance());
	UGamingUICpp* GamingUICpp = Cast<UGamingUICpp>(CurrentGameInstance->GetMenuUI());
	GamingUICpp->SetDemon(nullptr);
	Destroy();
}

