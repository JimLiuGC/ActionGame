// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HPComponentBaseCpp.generated.h"

class APlayerStateBaseCpp;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ACTIONGAME_API UHPComponentBaseCpp : public UActorComponent
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	bool DefenceEnabled;

	UPROPERTY()
	APlayerStateBaseCpp* CurrentPlayerState;
	
public:	
	// Sets default values for this component's properties
	UHPComponentBaseCpp();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/**
	 * 调用PlayerState减少玩家HP，若DefenceEnabled为true，则设置DefenceEnabled为false并退出该函数，不会调用PlayerState减少HP
	 * @param Down float，指定HP的减少量
	 */
	inline void DecreaseHP(const float Down);

	/**
	 * 设置是否使用防御，由技能系统间接调用
	 * @param EnableDefence bool，是否使用防御
	 */
	inline void SetEnableDefence(const bool EnableDefence);
};
