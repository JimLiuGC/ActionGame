// Fill out your copyright notice in the Description page of Project Settings.


#include "MapButtonCpp.h"
#include "GameInstanceBaseCpp.h"

UMapButtonCpp::UMapButtonCpp(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}

bool UMapButtonCpp::Initialize()
{
	Super::Initialize();
	UWidget* MapButtonBase = GetWidgetFromName("MapButton");
	if (MapButtonBase == nullptr)
	{
		return false;
	}
	UButton* MapButton = Cast<UButton>(MapButtonBase);
	if (MapButton == nullptr)
	{
		return false;
	}
	FScriptDelegate MapButtonDelegate;
	MapButtonDelegate.BindUFunction(this, FName("OnMapBtnCallback"));
	MapButton->OnClicked.Add(MapButtonDelegate);
	return true;
}


void UMapButtonCpp::SetMapId(const int ID)
{
	MapId = ID;
	UWidget* MapIdTextBase = GetWidgetFromName(FName("MapIdText"));
	if (MapIdTextBase == nullptr)
	{
		return;
	}
	UTextBlock* MapIdText = Cast<UTextBlock>(MapIdTextBase);
	if (MapIdText == nullptr)
	{
		return;
	}
	MapIdText->SetText(UKismetTextLibrary::Conv_IntToText(ID));
}

int UMapButtonCpp::GetMapId() const
{
	return MapId;
}

void UMapButtonCpp::OnMapBtnCallback()
{
	UGameInstanceBaseCpp* CurrentGameInstance = Cast<UGameInstanceBaseCpp>(GetGameInstance());
	CurrentGameInstance->LoadMapByMapId(MapId);
}

