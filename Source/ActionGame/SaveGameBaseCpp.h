// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerStateBaseCpp.h"
#include "SaveGameBaseCpp.generated.h"

enum class EItemType : uint8;
//struct FCollectableItemStruct;

/**
 * 
 */
UCLASS()
class ACTIONGAME_API USaveGameBaseCpp : public USaveGame
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	int Reward;

	UPROPERTY()
	float PlayerHP;

	UPROPERTY()
	int MapId;

	UPROPERTY()
	TMap<EItemType, FCollectableItemStruct> ItemsMap;

	FString SlotName;

public:
	USaveGameBaseCpp();

public:
	UFUNCTION(BlueprintCallable)
	void SaveData(const int& SavedMapId, const float& CurrentPlayerHP, const TMap<EItemType, FCollectableItemStruct>& SavedItemsMap);
	inline int GetReward() const;
	inline int GetMapId() const;
	inline void GetSavedItemsMap(TMap<EItemType, FCollectableItemStruct>& OutItemMap) const;
	
};
