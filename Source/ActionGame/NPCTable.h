// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "CoreMinimal.h"

#include "CollectableItemBaseCpp.h"
#include "HeaderDataCpp.h"
#include "NPCTable.generated.h"

USTRUCT(BlueprintType)
struct FNPCTable : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FString NPCId;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	EPawnType NPCType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float HP;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FVector LeftSide;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FVector RightSide;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FVector SpawnLocation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FVector SpawnDirection;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int ParentId;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	EItemType ItemType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int DropAmount;
};

/**
 * 
 */
class ACTIONGAME_API NPCTable
{
public:
	NPCTable();
	~NPCTable();
};
