// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WeaponBaseCpp.h"
#include "Kismet/GameplayStatics.h"
#include "KnifeBaseCpp.generated.h"

/**
 * 
 */
UCLASS()
class ACTIONGAME_API AKnifeBaseCpp : public AWeaponBaseCpp
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float FlySpeed;

	UPROPERTY()
	FVector FlyDirection;

	UPROPERTY()
	bool Lock;
public:
	AKnifeBaseCpp();
	virtual void Tick(float DeltaSeconds) override;

	/**
	 * 设置飞行运动的参数，设置完成后会将Lock置为false，即解锁
	 * @param Location FVector，出生点
	 * @param Direction FVector，飞行方向
	 */
	void SetFlyParam(const FVector Location, const FVector Direction);

	/**
	 * 设置锁，当且仅当从外部归还至对象池时使用
	 */
	void SetLock();

	UFUNCTION()
    void OnPlayerOverlapStart(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintImplementableEvent)
	void SetKnifeDirection(const int X);

	UFUNCTION()
	void SetMagicColor(const bool Use) const;
protected:
	virtual void BeginPlay() override;
};
