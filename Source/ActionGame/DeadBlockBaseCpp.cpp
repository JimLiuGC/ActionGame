// Fill out your copyright notice in the Description page of Project Settings.


#include "DeadBlockBaseCpp.h"
#include "PlayerBaseCpp.h"

// Sets default values
ADeadBlockBaseCpp::ADeadBlockBaseCpp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collider"));
	BoxCollider->SetSimulatePhysics(false);
	BoxCollider->SetEnableGravity(false);
	BoxCollider->SetGenerateOverlapEvents(true);
	RootComponent = BoxCollider;

	SpriteComponent = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("Sprite"));
	SpriteComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ADeadBlockBaseCpp::BeginPlay()
{
	Super::BeginPlay();
	FScriptDelegate DeadOverlapStartDelegate;
	DeadOverlapStartDelegate.BindUFunction(this, "OnPlayerOverlapStart");
	BoxCollider->OnComponentBeginOverlap.Add(DeadOverlapStartDelegate);
}

// Called every frame
void ADeadBlockBaseCpp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADeadBlockBaseCpp::OnPlayerOverlapStart(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor->ActorHasTag("Player"))
	{
		APlayerBaseCpp* CurrentPlayer = Cast<APlayerBaseCpp>(OtherActor);
		UGameplayStatics::ApplyDamage(CurrentPlayer, 1000.0f, nullptr, this, nullptr);
	}
}

