// Fill out your copyright notice in the Description page of Project Settings.


#include "GamingUICpp.h"
#include "PlayerStateBaseCpp.h"
#include "GameInstanceBaseCpp.h"
#include "ItemGridCpp.h"
#include "DemonBaseCpp.h"

bool UGamingUICpp::Initialize()
{
	Super::Initialize();
	
	UWidget* MenuPanelBase = GetWidgetFromName(FName("MenuPanel1"));
	if (MenuPanelBase == nullptr)
	{
		return false;
	}
	MenuPanel = Cast<UCanvasPanel>(MenuPanelBase);
	if (MenuPanel == nullptr)
	{
		return false;
	}
	MenuPanel->SetVisibility(ESlateVisibility::Collapsed);

	UWidget* DeadPanelBase = GetWidgetFromName(FName("DeadPanel"));
	if (DeadPanelBase == nullptr)
	{
		return false;
	}
	DeadPanel = Cast<UCanvasPanel>(DeadPanelBase);
	if (DeadPanel == nullptr)
	{
		return false;
	}
	DeadPanel->SetVisibility(ESlateVisibility::Collapsed);

	UWidget* InventoryPanelBase = GetWidgetFromName(FName("ItemPanel"));
	if (InventoryPanelBase == nullptr)
	{
		return false;
	}
	InventoryPanel = Cast<UCanvasPanel>(InventoryPanelBase);
	if (InventoryPanel == nullptr)
	{
		return false;
	}
	InventoryPanel->SetVisibility(ESlateVisibility::Collapsed);

	UWidget* GridManagerBase = GetWidgetFromName(FName("ItemGrid_BP"));
	if (GridManagerBase == nullptr)
	{
		return false;
	}
	GridManager = Cast<UItemGridCpp>(GridManagerBase);
	if (GridManager == nullptr)
	{
		return false;
	}
	UWidget* BackButtonBase = GetWidgetFromName(FName("BackButton"));
	if (BackButtonBase == nullptr)
	{
		return false;
	}
	UButton* BackButton = Cast<UButton>(BackButtonBase);
	if (BackButton == nullptr)
	{
		return false;
	}

	FScriptDelegate BackBtnDelegate;
	BackBtnDelegate.BindUFunction(this, FName("OnBackBtnCallback"));
	BackButton->OnClicked.Add(BackBtnDelegate);

	UWidget* PowerSliderBase = GetWidgetFromName(FName("PowerBar"));
	if (PowerSliderBase == nullptr)
	{
		return false;
	}
	PowerSlider = Cast<UProgressBar>(PowerSliderBase);
	if (PowerSlider == nullptr)
	{
		return false;
	}
	PowerSlider->PercentDelegate.BindUFunction(this, "GetPowerFromPlayerState");

	UWidget* HPSliderBase = GetWidgetFromName(FName("HPBar"));
	if (HPSliderBase == nullptr)
	{
		return false;
	}
	HPSlider = Cast<UProgressBar>(HPSliderBase);
	if (HPSlider == nullptr)
	{
		return false;
	}
	HPSlider->PercentDelegate.BindUFunction(this, "GetHPFromPlayerState");

	UWidget* NPCHPSliderBase = GetWidgetFromName(FName("NPCHPBar"));
	if (NPCHPSliderBase == nullptr)
	{
		return false;
	}
	NPCHPSlider = Cast<UProgressBar>(NPCHPSliderBase);
	if (NPCHPSlider == nullptr)
	{
		return false;
	}
	NPCHPSlider->SetVisibility(ESlateVisibility::Collapsed);
	NPCHPSlider->PercentDelegate.BindUFunction(this, "GetNPCHP");
	return true;
}

void UGamingUICpp::ShowReward()
{
	CurrentPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<APlayerStateBaseCpp>();
	UGameInstanceBaseCpp* CurrentGameInstance = Cast<UGameInstanceBaseCpp>(GetGameInstance());
	RewardText->SetText(UKismetTextLibrary::Conv_IntToText(CurrentGameInstance->GetTotalReward() + CurrentPlayerState->GetReward()));
}

void UGamingUICpp::OpenPanel(UCanvasPanel* TargetPanel)
{
	if (TargetPanel == nullptr)
	{
		return;
	}
	TargetPanel->SetVisibility(ESlateVisibility::Visible);
	if (PanelStack.Num() == 0)
	{
		PanelStack.Emplace(TargetPanel);
	}
}

void UGamingUICpp::ClosePanel()
{
	if (PanelStack.Num() != 0)
	{
		UCanvasPanel* TopPanel = PanelStack.Last();
		PanelStack.Pop();
		if (TopPanel != nullptr)
		{
			TopPanel->SetVisibility(ESlateVisibility::Collapsed);
		}
		TopPanel = nullptr;
	}
}

void UGamingUICpp::OnBackBtnCallback() const
{
	UGameInstanceBaseCpp* CurrentGameInstance = Cast<UGameInstanceBaseCpp>(GetGameInstance());
	CurrentGameInstance->BackToTitleMap();
}

void UGamingUICpp::ShowDeadPanel() const
{
	if (DeadPanel->GetVisibility() == ESlateVisibility::Collapsed)
	{
		DeadPanel->SetVisibility(ESlateVisibility::Visible);
	}
}

UCanvasPanel* UGamingUICpp::GetMenuPanel() const
{
	return MenuPanel;
}

TArray<UCanvasPanel*> UGamingUICpp::GetPanelStack() const
{
	return PanelStack;
}

UCanvasPanel* UGamingUICpp::GetInventoryPanel() const
{
	return InventoryPanel;
}

UItemGridCpp* UGamingUICpp::GetGridManager() const
{
	return GridManager;
}

float UGamingUICpp::GetPowerFromPlayerState()
{
	CurrentPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<APlayerStateBaseCpp>();
	return CurrentPlayerState->GetPower() / 100.0f;
}

float UGamingUICpp::GetHPFromPlayerState()
{
	CurrentPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<APlayerStateBaseCpp>();
	return CurrentPlayerState->GetPlayerHP() / 100.0f;
}

float UGamingUICpp::GetNPCHP()
{
	if (Demon)
	{
		const float DemonHP = Demon->GetCurrentHP();
		const float TotalHP = Demon->GetHP();
		return DemonHP / TotalHP;
	}
	return 0.0f;
}

UProgressBar* UGamingUICpp::GetNPCHPSlider() const
{
	return NPCHPSlider;
}

void UGamingUICpp::SetDemon(AActor* DemonActor)
{
	if (DemonActor == nullptr)
	{
		Demon = nullptr;
		NPCHPSlider->SetVisibility(ESlateVisibility::Collapsed);
		return;
	}
	Demon = Cast<ADemonBaseCpp>(DemonActor);
}

