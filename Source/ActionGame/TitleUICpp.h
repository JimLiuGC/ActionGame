// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/CanvasPanel.h"
#include "Kismet/KismetStringLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "TitleUICpp.generated.h"

class UMapButtonCpp;

/**
 * 
 */
UCLASS()
class ACTIONGAME_API UTitleUICpp : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY()
	TArray<UMapButtonCpp*> MapBtnArray;

protected:
	UPROPERTY()
	UCanvasPanel* MainPanel;

	UPROPERTY()
	UCanvasPanel* MapPanel;

	UPROPERTY()
	UCanvasPanel* InfoPanel;
	
	UPROPERTY()
	TArray<UCanvasPanel*> PanelStack;

public:
	UTitleUICpp(const FObjectInitializer& ObjectInitializer);

	virtual bool Initialize() override;

public:

	/**
	 * 打开关卡面板按钮的回调函数
	 */
	UFUNCTION()
	void OnStartBtnCallback();

	/**
	 * Info面板按钮的回调函数
	 */
	UFUNCTION()
	void OnInfoBtnCallback();

	/**
	 * 退出游戏按钮的回调函数
	 */
	UFUNCTION()
	void OnExitBtnCallback();

	/**
	 * 打开指定面板，操作面板栈，指定面板由GetXXXPanel获得
	 */
	void OpenPanel(UCanvasPanel* TargetPanel);

	/**
	 * 关闭最上层面板，操作面板栈
	 */
	void ClosePanel();

	/**
	 * 返回Map面板
	 * @return MapPanel 返回Map面板
	 */
	UCanvasPanel* GetMapPanel() const;

	/**
	 * 返回Info面板
	 * @return InfoPanel 返回Info面板
	 */
	UCanvasPanel* GetInfoPanel() const;

	/**
	 * 返回面板栈
	 * @return PanelStack 返回面板栈
	 */
	TArray<UCanvasPanel*> GetPanelStack() const;
	
};
