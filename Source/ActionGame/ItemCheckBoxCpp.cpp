// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemCheckBoxCpp.h"
#include "ItemCombinationPanelCpp.h"
#include "ItemGridCpp.h"
#include "PlayerStateBaseCpp.h"

UItemCheckBoxCpp::UItemCheckBoxCpp(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}


bool UItemCheckBoxCpp::Initialize()
{
	Super::Initialize();

	UWidget* CheckBoxBase = GetWidgetFromName("SubCheckBox");
	if (CheckBoxBase == nullptr)
	{
		return false;
	}
	CheckBox = Cast<UCheckBox>(CheckBoxBase);
	if (CheckBox == nullptr)
	{
		return false;
	}

	UWidget* ItemCntTextBlockBase = GetWidgetFromName(FName("ItemCntText"));
	if (ItemCntTextBlockBase == nullptr)
	{
		return false;
	}
	ItemCntTextBlock = Cast<UTextBlock>(ItemCntTextBlockBase);
	if (ItemCntTextBlock == nullptr)
	{
		return false;
	}

	UWidget* UncheckedBGImageBase = GetWidgetFromName(FName("UnCheckedBackImage"));
	if (UncheckedBGImageBase == nullptr)
	{
		return false;
	}
	UncheckedBGImage = Cast<UImage>(UncheckedBGImageBase);
	if (UncheckedBGImage == nullptr)
	{
		return false;
	}
	
	UWidget* CheckedBGImageBase = GetWidgetFromName(FName("CheckedBackImage"));
	if (CheckedBGImageBase == nullptr)
	{
		return false;
	}
	CheckedBGImage = Cast<UImage>(CheckedBGImageBase);
	if (CheckedBGImage == nullptr)
	{
		return false;
	}
	
	FScriptDelegate BoxCheckedDelegate;
	BoxCheckedDelegate.BindUFunction(this, "OnBoxChanged");
	CheckBox->OnCheckStateChanged.Add(BoxCheckedDelegate);
	CheckBox->SetIsEnabled(false);
	return true;
}

void UItemCheckBoxCpp::OnBoxChanged(bool State)
{
	if (State)
	{
		Parent->SetCurrChecked(this);
		if (Parent->GetPrevChecked() != Parent->GetCurrChecked())
		{
			Parent->GetPrevChecked()->GetCheckBox()->SetIsChecked(false);
			Parent->GetPrevChecked()->SetBGImage(false);
			Parent->SetPrevChecked(this);
			Parent->Refresh();
		}
	}
	else
	{
		Parent->GetCurrChecked()->GetCheckBox()->SetIsChecked(true);
	}
}

void UItemCheckBoxCpp::SetParentGrid(UItemGridCpp* ParentGrid)
{
	Parent = ParentGrid;
}

UCheckBox* UItemCheckBoxCpp::GetCheckBox() const
{
	return CheckBox;
}

void UItemCheckBoxCpp::SetHasItem(const bool Has, const int Cnt, const EItemType TargetType)
{
	HasItem = Has;
	CheckBox->SetIsEnabled(Has);
	if (Has)
	{
		ItemCntTextBlock->SetText(UKismetTextLibrary::Conv_IntToText(Cnt));
		Type = TargetType;
		SetCheckBoxStyle(CheckBox, TargetType, true);
	}
	else
	{
		ItemCntTextBlock->SetText(UKismetTextLibrary::Conv_StringToText(""));
		Type = EItemType::E_None;
		SetCheckBoxStyle(CheckBox, TargetType, false);
	}
}

bool UItemCheckBoxCpp::GetHasItem() const
{
	return HasItem;
}

void UItemCheckBoxCpp::SetBGImage(const bool State) const
{
	if (State)
	{
		UncheckedBGImage->SetVisibility(ESlateVisibility::Collapsed);
		CheckedBGImage->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
		UncheckedBGImage->SetVisibility(ESlateVisibility::Visible);
		CheckedBGImage->SetVisibility(ESlateVisibility::Collapsed);
	}
}

EItemType UItemCheckBoxCpp::GetItemType() const
{
	return Type;
}

