// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "HeaderDataCpp.h"
#include "CoreMinimal.h"
#include "ItemCombinationTable.generated.h"

USTRUCT(BlueprintType)
struct FItemCombinationTable : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	EItemType SourceItemType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	EItemType TargetItemType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int NeedAmount;
};

/**
 * 
 */
class ACTIONGAME_API ItemCombinationTable
{
public:
	ItemCombinationTable();
	~ItemCombinationTable();
};
