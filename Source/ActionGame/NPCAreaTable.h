// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "CoreMinimal.h"
#include "NPCAreaTable.generated.h"

USTRUCT(BlueprintType)
struct FNPCAreaTable : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int Id;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FVector SpawnLocation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float W;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float H;
};

/**
 * 
 */
class ACTIONGAME_API NPCAreaTable
{
public:
	NPCAreaTable();
	~NPCAreaTable();
};
