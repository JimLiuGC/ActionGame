// Fill out your copyright notice in the Description page of Project Settings.


#include "HPComponentBaseCpp.h"
#include "PlayerStateBaseCpp.h"

// Sets default values for this component's properties
UHPComponentBaseCpp::UHPComponentBaseCpp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHPComponentBaseCpp::BeginPlay()
{
	Super::BeginPlay();

	// ...
	CurrentPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<APlayerStateBaseCpp>();
}


// Called every frame
void UHPComponentBaseCpp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHPComponentBaseCpp::DecreaseHP(const float Down)
{
	if (DefenceEnabled)
	{
		DefenceEnabled = false;
		return;
	}
	CurrentPlayerState->DecreasePlayerHP(Down);
}

void UHPComponentBaseCpp::SetEnableDefence(const bool EnableDefence)
{
	DefenceEnabled = EnableDefence;
}

