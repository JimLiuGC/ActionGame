// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NPCBaseCpp.h"
#include "SmallGoblinBaseCpp.generated.h"

/**
 * 
 */
UCLASS()
class ACTIONGAME_API ASmallGoblinBaseCpp : public ANPCBaseCpp
{
	GENERATED_BODY()
public:
	ASmallGoblinBaseCpp();
protected:
	virtual void BeginPlay() override;
public:
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetNPCDead();
	virtual void DeadTimerCallback() override;
};
