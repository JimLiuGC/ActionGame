// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NPCBaseCpp.h"
#include "DemonBaseCpp.generated.h"

class AGameModeBaseCpp;

/**
 * 
 */
UCLASS()
class ACTIONGAME_API ADemonBaseCpp : public ANPCBaseCpp
{
	GENERATED_BODY()
protected:
	UPROPERTY()
	bool Lock;

	UPROPERTY()
	AGameModeBaseCpp* CurrentGameMode;
public:
	ADemonBaseCpp();
protected:
	virtual void BeginPlay() override;
public:
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetNPCDead();
	
};
