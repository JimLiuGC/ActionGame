// Fill out your copyright notice in the Description page of Project Settings.


#include "SmallGoblinBaseCpp.h"
#include "NPCAreaBaseCpp.h"
#include "GameModeBaseCpp.h"

ASmallGoblinBaseCpp::ASmallGoblinBaseCpp()
{
	PrimaryActorTick.bCanEverTick = true;
	ActionState = ENPCAction::E_Idle;
	AnimState = ENPCAnim::E_IdleLeft;
	Tags.Add(FName("SmallGoblin"));
}


void ASmallGoblinBaseCpp::BeginPlay()
{
	Super::BeginPlay();
	AGameModeBaseCpp* CurrentGameMode = Cast<AGameModeBaseCpp>(UGameplayStatics::GetGameMode(GetWorld()));
	CurrentGameMode->GetPawnDamageMap(EPawnType::E_SmallGoblin, EAttackMode::E_BasicAttack, BaseDamage);
	CurrentGameMode->GetPawnDamageMap(EPawnType::E_SmallGoblin, EAttackMode::E_ProAttack, ProDamage);
}

void ASmallGoblinBaseCpp::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (locked || AbilityBusy)
	{
		return;
	}
	if (ActionState == ENPCAction::E_Walk)
	{
		Walk(DeltaSeconds);
	}
	else if (ActionState == ENPCAction::E_Run)
	{
		if (ParentArea != nullptr)
		{
			AActor* Player = ParentArea->GetPlayerInArea();
			if (Player != nullptr)
			{
				const bool IsTouchedPlayer = Run(DeltaSeconds, Player->GetActorLocation());
				if (IsTouchedPlayer)
				{
					Attack(0.2667f, 0.2f);
				}
			}
		}
	}
	else if (ActionState == ENPCAction::E_Attack)
	{
		UE_LOG(LogTemp, Warning, TEXT("Attack"));
	}
}

void ASmallGoblinBaseCpp::SetNPCDead()
{
	Super::SetDead();
	const FTimerDelegate DeadTimerDelegate = FTimerDelegate::CreateUObject(this, &ASmallGoblinBaseCpp::DeadTimerCallback);
	GetWorldTimerManager().SetTimer(DeadTimerHandle, DeadTimerDelegate, 0.33336f, false);
}

void ASmallGoblinBaseCpp::DeadTimerCallback()
{
	GetWorldTimerManager().ClearTimer(DeadTimerHandle);
	Destroy();
}
