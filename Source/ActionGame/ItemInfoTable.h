// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "HeaderDataCpp.h"
#include "CoreMinimal.h"
#include "ItemInfoTable.generated.h"

USTRUCT(BlueprintType)
struct FItemInfoTable : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	EItemType ItemType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText InfoText;
};

/**
 * 
 */
class ACTIONGAME_API ItemInfoTable
{
public:
	ItemInfoTable();
	~ItemInfoTable();
};
