// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HeaderDataCpp.h"
#include "Components/ActorComponent.h"
#include "Kismet/GameplayStatics.h"
#include "AttackComponentBaseCpp.generated.h"

class APlayerBaseCpp;
class APlayerStateBaseCpp;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ACTIONGAME_API UAttackComponentBaseCpp : public UActorComponent
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	APlayerBaseCpp* ParentPlayer;

	UPROPERTY()
	int ComboAmount;

	UPROPERTY()
	APlayerStateBaseCpp* CurrentPlayerState;

	UPROPERTY()
	bool UseMagicBuffer;

public:	
	// Sets default values for this component's properties
	UAttackComponentBaseCpp();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	void Attack(const TArray<FHitResult>& NPCs);

	UFUNCTION()
	void AttackPro() const;

	UFUNCTION()
	void ApplyDamage(AActor* DamageTaker, const EAttackMode AttackMode);

	UFUNCTION()
	void SetParentPlayer(APlayerBaseCpp* Player);

	UFUNCTION()
	void SetUseMagicBuffer(const bool Use);

	UFUNCTION()
	bool GetUseMagicBuffer() const;
};
