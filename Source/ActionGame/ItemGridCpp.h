// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HeaderDataCpp.h"
#include "CollectableItemBaseCpp.h"
#include "Blueprint/UserWidget.h"
#include "Components/CheckBox.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Kismet/KismetStringLibrary.h"
#include "ItemGridCpp.generated.h"

class UItemCheckBoxCpp;
class UItemCombinationPanelCpp;
class APlayerBaseCpp;

/**
 * 
 */
UCLASS()
class ACTIONGAME_API UItemGridCpp : public UUserWidget
{
	GENERATED_BODY()
protected:
	UPROPERTY()
	TArray<UItemCheckBoxCpp*> ItemArray;

	UPROPERTY()
	UTextBlock* SelectedItemInfoText;

	UPROPERTY()
	UItemCombinationPanelCpp* CombinationPanel;

	UPROPERTY()
	UItemCheckBoxCpp* PrevChecked;

	UPROPERTY()
	UItemCheckBoxCpp* CurrentChecked;

	UPROPERTY()
	UButton* UseButton;

public:
	UItemGridCpp(const FObjectInitializer& ObjectInitializer);

	virtual bool Initialize() override;

	/**
	 * 设置旧指针
	 * @param Prev UItemCheckBoxCpp，旧指针 
	 */
	void SetPrevChecked(UItemCheckBoxCpp* Prev);

	/**
	 * 返回旧指针
	 * @return PrevChecked UItemCheckBoxCpp，返回的旧指针
	 */
	UItemCheckBoxCpp* GetPrevChecked() const;

	/**
	 * 设置新指针
	 * @param Curr UItemCheckBoxCpp，新指针
	 */
	void SetCurrChecked(UItemCheckBoxCpp* Curr);

	/**
	 * 返回新指针
	 * @return CurrentChecked UItemCheckBoxCpp，返回的新指针
	 */
	UItemCheckBoxCpp* GetCurrChecked() const;

	/**
	 * 滞后初始化函数，若已经初始化了，则执行Refresh，否则执行默认初始化的相关操作，设置新旧指针，并Reset。
	 */
	void Init();

	/**
	 * 刷新函数，在Init中调用。
	 * 从PlayerStateBaseCpp中获取不同大类的物品Map，将不同大类的物品更新到ItemArray中。
	 */
	void Refresh();

	/**
	 * 复位函数，将ItemArray中所有含有物品的checkbox都设置为没有物品。
	 */
	void Reset();

	/**
	 * 设置物品描述的文字
	 * @param ItemType EItemType，物品类型
	 */
	UFUNCTION(BlueprintImplementableEvent)
	void SetItemInfoText(const EItemType& ItemType);

	/**
	 * 返回ItemCombinationPanel
	 * @return CombinationPanel UItemCombinationPanelCpp，返回的ItemCombinationPanel
	 */
	UItemCombinationPanelCpp* GetItemCombinationPanel() const;

	/**
	 * 使用物品按钮的回调函数。
	 */
	UFUNCTION()
	void UseButtonCallback();
};
