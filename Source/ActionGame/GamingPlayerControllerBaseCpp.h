// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GamingPlayerControllerBaseCpp.generated.h"

/**
 * 
 */
UCLASS()
class ACTIONGAME_API AGamingPlayerControllerBaseCpp : public APlayerController
{
	GENERATED_BODY()

private:
	bool IsMenuOn = false;
public:
	virtual void SetupInputComponent() override;
	
public:
	/**
	 * 输入事件“移动”的回调函数，通过CheckPanelStack检查是否有正在显示的面板
	 */
	virtual void MoveRightCallback(float Axis);

	/**
	* 输入事件“跳跃”的回调函数，通过CheckPanelStack检查是否有正在显示的面板
	*/
	virtual void MoveJumpCallback();

	/**
	* 输入事件“攻击”的回调函数
	*/
	virtual void AttackCallback();

	/**
	 * 输入事件“高级攻击”的回调函数
	 */
	virtual void AttackProCallback();

	/**
	* 输入事件“显示菜单”的回调函数，使用面板栈打开面板
	*/
	virtual void MenuCallback();

	/**
	* 输入事件“返回”的回调函数，使用面板栈关闭面板
	*/
	virtual void BackCallback();

	/**
	 * 输入事件“背包”的回调函数，使用面板栈
	 */
	virtual void InventoryCallback();
protected:

	/**
	 * 使用面板栈检查当前是否有正在显示的面板
	 */
	bool CheckPanelStack() const;
};
