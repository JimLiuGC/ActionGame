// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnBaseCpp.h"

// Sets default values
APawnBaseCpp::APawnBaseCpp()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APawnBaseCpp::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APawnBaseCpp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APawnBaseCpp::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APawnBaseCpp::RayTrace(const FVector& StartPoint, const FVector EndPoint, FHitResult& HitResult)
{
	FCollisionQueryParams CollisionQueryParams(FName(TEXT("RayTrace")), true, this);
	CollisionQueryParams.bTraceComplex = false;
	CollisionQueryParams.bReturnPhysicalMaterial = false;
	GetWorld()->LineTraceSingleByChannel(HitResult, StartPoint, EndPoint, ECC_Visibility, CollisionQueryParams);
}

void APawnBaseCpp::RayTraceMulti(const FVector& StartPoint, const FVector EndPoint, TArray<FHitResult>& HitResults)
{
	FCollisionQueryParams CollisionQueryParams(FName(TEXT("RayTraceMulti")), false, this);
	CollisionQueryParams.bTraceComplex = false;
	CollisionQueryParams.bReturnPhysicalMaterial = false;
	CollisionQueryParams.bIgnoreBlocks = false;
	GetWorld()->LineTraceMultiByChannel(HitResults, StartPoint, EndPoint, ECC_Visibility, CollisionQueryParams);
}

float APawnBaseCpp::GetHP() const
{
	return HP;
}

float APawnBaseCpp::GetCurrentHP() const
{
	return CurrentHP;
}

void APawnBaseCpp::SetInitHP(const float InitHP)
{
	HP = InitHP;
	CurrentHP = InitHP;
}

void APawnBaseCpp::DecreaseCurrentHP(const float DeltaValue)
{
	CurrentHP -= DeltaValue;
}

