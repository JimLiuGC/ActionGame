// Fill out your copyright notice in the Description page of Project Settings.


#include "UIPlayerControllerBaseCpp.h"
#include "GameInstanceBaseCpp.h"
#include "TitleUICpp.h"

void AUIPlayerControllerBaseCpp::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction("Back", IE_Pressed, this, &AUIPlayerControllerBaseCpp::BackCallback);
}

void AUIPlayerControllerBaseCpp::BackCallback()
{
	UGameInstanceBaseCpp* CurrentGameInstance = Cast<UGameInstanceBaseCpp>(GetGameInstance());
	UTitleUICpp* TitleUIWidget = Cast<UTitleUICpp>(CurrentGameInstance->GetTitleUI());
	if (TitleUIWidget == nullptr)
	{
		return;
	}
	if (TitleUIWidget->GetPanelStack().Num() != 0)
	{
		TitleUIWidget->ClosePanel();
	}
}

