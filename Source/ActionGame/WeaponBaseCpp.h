// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PaperFlipbookComponent.h"
#include "Components/BoxComponent.h"
#include "WeaponBaseCpp.generated.h"

class APlayerStateBaseCpp;

UCLASS()
class ACTIONGAME_API AWeaponBaseCpp : public AActor
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPaperFlipbookComponent* SpriteComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBoxComponent* BoxCollider;

	UPROPERTY()
	APlayerStateBaseCpp* CurrentPlayerState;

	UPROPERTY()
	float WeaponDamage;
	
public:	
	// Sets default values for this actor's properties
	AWeaponBaseCpp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	inline void SetWeaponDamage(const float Value);

};
